<?php

class Vendre_direct {
	private $pdo;

	public function __construct($dbPdo){
	
	$this->pdo=$dbPdo;
}
    public function modifbddInsert($tab){

		$req=$this->pdo->prepare('INSERT INTO proprietaire_maison(nom,prenom,adresse,cp,ville) 
			VALUES (:nom,:prenom,:adresse,:cp,:ville)');

		$req->bindParam(':nom',$tab['nom'],PDO::PARAM_STR);
		$req->bindParam(':prenom',$tab['prenom'],PDO::PARAM_STR);
		$req->bindParam(':adresse',$tab['adresse'],PDO::PARAM_STR);
		$req->bindParam(':cp',$tab['cp'],PDO::PARAM_STR);
		$req->bindParam(':ville',$tab['ville'],PDO::PARAM_STR);
		
		$req->execute();
	}

	public function modifbddSelect(){
		
		$sql=('SELECT * FROM proprietaire_maison');
		return $this->pdo->query($sql)->fetchAll();

	}

	public function modifbddPro($tab){		
		
		$req=$this->pdo->prepare('UPDATE proprietaire_maison SET nom=:nom,prenom=:prenom,adresse=:adresse,cp=:cp,ville=:ville
			WHERE id_proprietaire=:id');

		$req->bindParam(':id',$tab['id'],PDO::PARAM_INT);
		$req->bindParam(':nom',$tab['nom'],PDO::PARAM_STR);
		$req->bindParam(':prenom',$tab['prenom'],PDO::PARAM_STR);
		$req->bindParam(':adresse',$tab['adresse'],PDO::PARAM_STR);
		$req->bindParam(':cp',$tab['cp'],PDO::PARAM_STR);
		$req->bindParam(':ville',$tab['ville'],PDO::PARAM_STR);
		
		$req->execute();
			
	}


}


?>