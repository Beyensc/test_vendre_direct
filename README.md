# **Test php**

-----


Créer une application HMVC qui permet de faire un select et un insert/update dans une bd mysql et dans un fichier CSV.

Le but étant que les fonctions soient les mêmes et qu'il n'y est juste le code qui change en appliquant le principe d'interface pour la connexion BDD.

Données a insérer.

Propriétaire de maison

Nom  
Prénom  
Adresse  
Code postal  
Ville  

*bdd : OK  
*select/insert/update bd mysql : OK  
*select/insert/update fichier CSV : OK  
*interface : En cours 

## Sources :  
*[CSV](http://www.coursinformatique.net/php/creer-un-fichier-csv-avec-php.html)  
*[Interfaces openclassroom](https://openclassrooms.com/courses/programmez-en-oriente-objet-en-php/les-interfaces-1)  
*[Interfaces php.net](http://php.net/manual/fr/language.oop5.interfaces.php)

