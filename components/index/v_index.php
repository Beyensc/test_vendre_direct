<?php

class Vindex extends VBase {

    function __construct($appli, $model) {
        parent::__construct($appli, $model);
    }

    public function formulaire(){

    	$html ='';
    	$html.='<table class="table">
    				<legende>Propiétaire maison</legende>
    				<tr><td>Nom</td><td><input type="text" name="nom"  id="nom" placeholder="Nom"/></td></tr>
    				<tr><td>Prénom</td><td><input type="text" name="prenom" id="prenom" placeholder="Prénom"/></td></tr>
    				<tr><td>Adrese</td><td><input type="text" name="adresse" id="adresse" placeholder="Adresse"/></td></tr>
    				<tr><td>Code postal</td><td><input type="text" name="cp" id="cp" placeholder="Code Postal"/></td></tr>
    				<tr><td>Ville</td><td><input type="text" name="ville" id="ville" placeholder="Ville"/></td></tr>
    				<tr><td><input type="button" name="valider" value="Valider" id="valider" onclick="modifbdd();"</td></tr>
                    <tr><td><a href="proprietaire_maison.csv" download="proprietaire_maison.csv">Télécharger le fichier .CSV</a></td></tr>
    			</table>';

    	$this->appli->formulaire=$html;


    }

    public function proprietaire($modifbddSelect){
    	$html='';
    	

    				foreach ($modifbddSelect as $key => $value) {

    		$html.='<table class="table" id="form_list_proprio'.$value['id_proprietaire'].'">
    				<legende>Propiétaire maison</legende>
    				

    				<tr><td>Nom</td><td><input type="text" name="nom"  id="nom'.$value['id_proprietaire'].'" value="'.ucfirst($value['nom']).'"/></td></tr>
    				<tr><td>Prénom</td><td><input type="text" name="prenom" id="prenom'.$value['id_proprietaire'].'" value="'.ucfirst($value['prenom']).'"/></td></tr>
    				<tr><td>Adrese</td><td><input type="text" name="adresse" id="adresse'.$value['id_proprietaire'].'" value="'.$value['adresse'].'"/></td></tr>
    				<tr><td>Code postal</td><td><input type="text" name="cp" id="cp'.$value['id_proprietaire'].'" value="'.$value['cp'].'"/></td></tr>
    				<tr><td>Ville</td><td><input type="text" name="ville" id="ville'.$value['id_proprietaire'].'" value="'.ucfirst($value['ville']).'"/></td></tr>
    				<tr><td><input type="button" name="modifier" value="Modifier" id="valider'.$value['id_proprietaire'].'" onclick="modifbddPro(\''.$value['id_proprietaire'].'\',\''.$value['nom'].'\');"</td></tr>
    			</table>';
    		}

    	$this->appli->proprietaire_maison=$html;
    }

    public function listCSV($modifbddSelect){

        // Les lignes du tableau
        foreach ($modifbddSelect as $key => $value) {
            
            $lignes[] = array($value['nom'],$value['prenom'],$value['adresse'],$value['cp'],$value['ville']);
        }


            // Paramétrage de l'écriture du futur fichier CSV
            $chemin = 'proprietaire_maison.csv';
            $delimiteur = ','; // Pour une tabulation, utiliser $delimiteur = "t";

            // Création du fichier csv (le fichier est vide pour le moment)
            // w+ : consulter http://php.net/manual/fr/function.fopen.php
            $fichier_csv = fopen($chemin, 'w+');

            // Si votre fichier a vocation a être importé dans Excel,
            // vous devez impérativement utiliser la ligne ci-dessous pour corriger
            // les problèmes d'affichage des caractères internationaux (les accents par exemple)
            fprintf($fichier_csv, chr(0xEF).chr(0xBB).chr(0xBF));

            // Boucle foreach sur chaque ligne du tableau
            foreach($lignes as $ligne){
                // chaque ligne en cours de lecture est insérée dans le fichier
                // les valeurs présentes dans chaque ligne seront séparées par $delimiteur
                fputcsv($fichier_csv, $ligne, $delimiteur);
            }

            // fermeture du fichier csv
            fclose($fichier_csv);
    }
	
}
?>