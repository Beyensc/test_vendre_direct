-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Jeu 14 Juillet 2016 à 09:38
-- Version du serveur: 5.5.49-0ubuntu0.14.04.1
-- Version de PHP: 5.5.37-1+deprecated+dontuse+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `vendre_direct`
--

-- --------------------------------------------------------

--
-- Structure de la table `proprietaire_maison`
--

CREATE TABLE IF NOT EXISTS `proprietaire_maison` (
  `id_proprietaire` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(40) NOT NULL,
  `prenom` varchar(40) NOT NULL,
  `adresse` varchar(100) NOT NULL,
  `cp` int(10) NOT NULL,
  `ville` varchar(40) NOT NULL,
  PRIMARY KEY (`id_proprietaire`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `proprietaire_maison`
--

INSERT INTO `proprietaire_maison` (`id_proprietaire`, `nom`, `prenom`, `adresse`, `cp`, `ville`) VALUES
(2, 'Beyens', 'ClÃ©ment', 'rue du dragon ', 7700, 'Mouscron'),
(3, 'durand', 'arnaud', 'rachel-lagast', 7711, 'mouscron'),
(4, 'Dupont', 'Bernard', 'rue du congo', 7711, 'Mouscron');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
